#!/bin/bash
if [[ -z ${1} ]]; then
  cd /root/
  python runserver.py > server.log
else
  exec $@
fi    
